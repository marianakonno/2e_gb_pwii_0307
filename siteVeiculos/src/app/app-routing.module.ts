import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MotosComponent } from './components/motos/motos.component';

const routes: Routes = [
  { path: "",
    loadChildren: () => import('./components/home/home.module').
    then(m => m.HomeModule)
  },
  { path: "login",
    loadChildren: () => import('./components/login/login.module').
    then(m => m.LoginModule)
  },
  { path: "motos",
    loadChildren: () => import('./components/motos/motos.module').
    then(m => m.MotosModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
